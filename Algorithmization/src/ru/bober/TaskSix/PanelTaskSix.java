package ru.bober.TaskSix;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;
import ru.bober.GUI.GSLable.GSHeaderLable;
import ru.bober.GUI.GSLable.GSLable;
import ru.bober.GUI.GSTextField.GSTextField;
import ru.bober.GUI.GSButton.GSButton;

public class PanelTaskSix extends JPanel
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private GSHeaderLable headerLable = new GSHeaderLable("Задание 6", 10, 11, 580, 83, 40);
	private GSLable str_info_lbl = new GSLable("Введите строку для анализа", 10, 105, 580, 50, 17);
	private GSTextField str_txt = new GSTextField(10, 166, 580, 50, 17);
	private GSLable word_lbl = new GSLable("Количество слов в строке: ", 10, 227, 580, 50, 17);
	private GSLable a_lbl = new GSLable("Количество букв \"а\" в последним слове: ", 10, 288, 580, 50, 17);
	private GSButton button = new GSButton("Анализировать", 10, 389, 580, 50, 17);

	/**
	 * Create the panel.
	 */
	public PanelTaskSix()
	{
		initialize();
	}
	private void initialize() {
		setPreferredSize(new Dimension(600, 477));
		setLayout(null);
		
		add(headerLable);
		add(str_info_lbl);
		add(str_txt);
		add(word_lbl);
		add(a_lbl);
		add(button);
		
		button.addActionListener(new BtnListener(str_txt, word_lbl, a_lbl));
	}
}
class BtnListener implements ActionListener
{

	GSTextField str_txt;
	GSLable word_lbl;
	GSLable a_lbl;
	public BtnListener(GSTextField str_txt, GSLable word_lbl, GSLable a_lbl)
	{
		this.str_txt = str_txt;
		this.word_lbl = word_lbl;
		this.a_lbl = a_lbl;
	}
	
	@Override
	public void actionPerformed(ActionEvent e)
	{
		TaskSix taskSix = new TaskSix(str_txt.getText());
		word_lbl.setText("Количество слов в строке: " + taskSix.wordCount);
		a_lbl.setText("Количество букв \"а\" в последним слове: " + taskSix.vowelsCount);
	}
	
}