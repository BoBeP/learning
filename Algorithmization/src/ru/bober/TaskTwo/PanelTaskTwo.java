package ru.bober.TaskTwo;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JPanel;
import javax.swing.SwingConstants;

import ru.bober.GUI.GSLable.GSHeaderLable;
import ru.bober.GUI.GSLable.GSLable;
import ru.bober.GUI.GSTextField.GSTextField;
import ru.bober.GUI.GSButton.GSButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class PanelTaskTwo extends JPanel
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private GSHeaderLable headerLable = new GSHeaderLable("Задание 2", 0, 11, 590, 100, 40);
	private GSLable x_lbl = new GSLable("Координата по X", 10, 122, 275, 50, 17);
	private GSTextField x_txt = new GSTextField(295, 122, 295, 50, 17);
	private GSLable y_lbl = new GSLable("Координата по Y", 10, 183, 275, 50, 17);
	private GSTextField y_txt = new GSTextField(295, 183, 295, 50, 17);
	private GSButton button = new GSButton("Результат", 10, 380, 580, 50, 17);
	private GSLable res_lbl = new GSLable(10, 244, 580, 125, 17);

	/**
	 * Create the panel.
	 */
	public PanelTaskTwo()
	{
		initialize();
	}
	private void initialize() {
		setPreferredSize(new Dimension(600, 477));
		setLayout(null);
		
		add(headerLable);
		
		add(x_lbl);
		add(x_txt);
		add(y_lbl);
		add(y_txt);
		add(button);
		add(res_lbl);
		
		res_lbl.setHorizontalAlignment(SwingConstants.CENTER);
		res_lbl.setVerticalAlignment(SwingConstants.CENTER);
		button.addActionListener(new Btnlistener(x_txt, y_txt, res_lbl));
	}
}

class Btnlistener implements ActionListener
{
	private GSTextField x_txt;
	private GSTextField y_txt;
	private GSLable res_lbl;
	
	float x, y;
	
	public Btnlistener(GSTextField x_txt, GSTextField y_txt, GSLable res_lbl)
	{
		this.x_txt = x_txt;
		this.y_txt = y_txt;
		this.res_lbl = res_lbl;
	}
	
	@Override
	public void actionPerformed(ActionEvent e)
	{
		
		
		
		res_lbl.setOpaque(true);
		x = Float.parseFloat(x_txt.getText().replace(",", "."));
		y=Float.parseFloat(y_txt.getText().replace(",", "."));
		
		TaskTwo taskTwo = new TaskTwo(x, y);
		if(taskTwo.check.equals("1"))
		{
			res_lbl.setBackground(Color.GREEN);
			res_lbl.setText("Точка входит");
		}else
		{
			res_lbl.setBackground(Color.RED);
			res_lbl.setText("Точка Невходит");
		}
	}
	
}