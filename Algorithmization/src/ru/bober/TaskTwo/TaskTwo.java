package ru.bober.TaskTwo;

/**
 * https://vk.com/im?peers=157204191_c18&sel=93820259&z=photo93820259_388061371%2Fmail113793
 * @author BoBeP
 *
 */
public class TaskTwo
{
	String check = "";
	
	public TaskTwo(float x, float y)
	{
		
		if
		(
			((x>=-0.5) && (x<=0) && (y<=2) && (y>=1)) //Прямоугольник
			||
			((Math.sqrt(x) + Math.sqrt(y) <= 1) && (x<=0) && (x>=-1) && (y<=1) && (y>=0))//Дуга слева
			||
			((((x*y)/2) <=0.05) && (x>=-1) && (x<=0) && (y>=-0.5) && (y<=0))//Треугольник слева
			|| 
			((((x*y)/2) <=1) &&  (x>=0) && (x<2) && (y>=0) && (y<1))// Треугольник справа
			||
			(((Math.sqrt(x) + Math.sqrt(y))<=2) && (x>0) && (x<2) && (y>-0.7) && (y<0))//Дуга которая входит
			|| 
			(((Math.sqrt(x) + Math.sqrt(y))<=2) && (x>=0) && (x<=2) && (y>-0.5) && (y<=0))//Дуга которая не входит
		)
		{
			check = "1";
		}else
		{
			check = "0";
		}
		
	}

}
