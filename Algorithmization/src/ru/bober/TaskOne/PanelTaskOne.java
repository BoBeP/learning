package ru.bober.TaskOne;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;
import javax.swing.SwingConstants;

import ru.bober.GUI.GSLable.GSLable;
import ru.bober.GUI.GSTextField.GSTextField;
import ru.bober.GUI.GSLable.GSHeaderLable;
import ru.bober.GUI.GSButton.GSButton;

public class PanelTaskOne extends JPanel
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4287404265398477597L;
	
	private GSHeaderLable headerLable = new GSHeaderLable("Задание 1", 10, 11, 580, 69, 50);
	private GSLable l0_lbl = new GSLable("Число L0", 10, 91, 130, 30, 17);
	private GSTextField l0_txt = new GSTextField(147, 91, 130, 30, 17);
	private GSLable l1_lbl = new GSLable("Число L1", 10, 132, 130, 30, 17);
	private GSTextField l1_txt = new GSTextField(147, 132, 130, 30, 17);
	private GSLable l2_lbl = new GSLable("Число L2", 10, 173, 130, 30, 17);
	private GSTextField l2_txt = new GSTextField(147, 173, 130, 30, 17);
	private GSLable r_lbl = new GSLable("Результат R", 10, 214, 130, 30, 17);
	private GSLable res_r_lbl = new GSLable(147, 214, 130, 30, 17);
	
	
	private GSLable a1_lbl = new GSLable("Число A1", 310, 91, 130, 30, 17);
	private GSTextField a1_txt = new GSTextField(450, 93, 130, 30, 17);
	private GSLable b_lbl = new GSLable("Число B", 310, 132, 130, 30, 17);
	private GSTextField b_txt = new GSTextField(450, 132, 130, 30, 17);
	private GSLable x_lbl = new GSLable("Число X", 310, 173, 130, 30, 17);
	private GSTextField x_txt = new GSTextField(450, 173, 130, 30, 17);
	private GSLable k_lbl = new GSLable("Число K", 310, 214, 130, 30, 17);
	private GSTextField k_txt = new GSTextField(450, 214, 130, 30, 17);
	private GSLable y_lbl = new GSLable("Результат Y", 310, 255, 130, 30, 17);
	private GSLable res_y_lbl = new GSLable(450, 255, 130, 30, 17);

	private GSButton res_btn = new GSButton("Результат", 10, 315, 580, 57, 17);
	private GSLable res_s_lbl = new GSLable(10, 383, 570, 69, 50);
	float l0, l1, l2, a1, b, x, k;
	/**
	 * Create the panel.
	 */
	public PanelTaskOne()
	{

		initialize();
	}
	private void initialize() 
	{
		setPreferredSize(new Dimension(600, 477));
		setLayout(null);
		add(headerLable);
		
		add(l0_lbl);
		add(l0_txt);
		add(l1_lbl);
		add(l1_txt);
		add(l2_lbl);
		add(l2_txt);
		add(r_lbl);
		add(res_r_lbl);
		
		add(a1_lbl);
		add(a1_txt);
		add(b_lbl);
		add(b_txt);
		add(x_lbl);
		add(x_txt);
		add(k_lbl);
		add(k_txt);
		add(y_lbl);
		add(res_y_lbl);
		
		add(res_btn);
		add(res_s_lbl);
		
		res_s_lbl.setHorizontalAlignment(SwingConstants.CENTER);
		res_s_lbl.setVerticalAlignment(SwingConstants.CENTER);
		
		res_btn.addActionListener(new BtnListener(l0_txt, l1_txt, l2_txt, a1_txt, b_txt, x_txt, k_txt, res_r_lbl, res_y_lbl, res_s_lbl));
	
	}
	
}

class BtnListener implements ActionListener
{
	float l0, l1, l2, a1, b, x, k;
	
	GSTextField l0_txt;
	GSTextField l1_txt;
	GSTextField l2_txt;
	GSTextField a1_txt;
	GSTextField b_txt;
	GSTextField x_txt;
	GSTextField k_txt;
	GSLable res_r_lbl;
	GSLable res_y_lbl;
	GSLable res_s_lbl;
	
	public BtnListener(GSTextField l0_txt, GSTextField l1_txt, GSTextField l2_txt, GSTextField a1_txt, GSTextField b_txt, GSTextField x_txt, GSTextField k_txt,  GSLable res_r_lbl, GSLable res_y_lbl, GSLable res_s_lbl)
	{
		this.l0_txt = l0_txt;
		this.l1_txt = l1_txt;
		this.l2_txt = l2_txt;
		this.a1_txt = a1_txt;
		this.b_txt = b_txt;
		this.x_txt = x_txt;
		this.k_txt = k_txt;
		
		this.res_r_lbl = res_r_lbl;
		this.res_y_lbl = res_y_lbl;
		this.res_s_lbl = res_s_lbl;
	}
	
	@Override
	public void actionPerformed(ActionEvent e)
	{
		l0 = Float.parseFloat(l0_txt.getText()); 
		l1 = Float.parseFloat(l1_txt.getText());
		l2 = Float.parseFloat(l2_txt.getText());
		a1 = Float.parseFloat(a1_txt.getText());
		b = Float.parseFloat(b_txt.getText());
		x = Float.parseFloat(x_txt.getText());
		k = Float.parseFloat(k_txt.getText());
		
		TaskOne taskOne = new TaskOne(l0, l1, l2, a1, b, x, k);
		res_r_lbl.setText(Float.toString(taskOne.r));
		res_y_lbl.setText(Float.toString(taskOne.y));
		res_s_lbl.setText(Float.toString(taskOne.s));
	}
}
