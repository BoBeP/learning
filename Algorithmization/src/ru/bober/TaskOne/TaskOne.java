package ru.bober.TaskOne;

import javax.swing.JOptionPane;

import ru.bober.GUI.GSLable.GSLable;

public class TaskOne
{
	float d1, d2, d3, d4, d5, d6, d7, d8;
	float s, r, y;
	
	
	public TaskOne(float l0, float l1, float l2, float a1, float b, float x, float k)
	{
		//Действие 1
		d1 = (float) Math.cos(Math.toRadians(Math.PI*Math.pow(l0, 3)));
		//Действие 2
		float abs = Math.abs(l1 - l2);
		float ln = (float) Math.log10(abs);
		d2 = (float) (3.2*Math.pow(10, 3) - Math.pow(l1, 3)*ln);
		
		if(!(d2 == 0))
		{
			//Действие 3
			d3 = d1/d2;
			
			//Действие 4
			d4 = l0*d3;
			
			//Действие 5
			r = (float) (d4 - Math.sin(Math.toRadians(Math.pow(l0, 2))));
			
			r = round(r, 3);
			
			//Действие 6 
			d5 = (float) (6.9-2.3*b*x);
			
			if(!(d5 == 0))
			{
				//Дейстиве 7
				d6 = (b*x)/d5;
				//Действие 9
				d8 = (float) (Math.pow(x, 4)*Math.cos(Math.toRadians(3*k)));
				
				if(k >= 0)
				{
					//Действие 8
					d7 = (float) Math.pow(Math.log(3*k), 3);
					
					y = a1 +d5+d6-d7;
					y = round(y, 3);
					s=r+y;
					
				}
				else
				{
					JOptionPane.showMessageDialog(null, new GSLable("Нельзя вычислить логарифм отрицательного числа", 17), "Ошибка", JOptionPane.ERROR_MESSAGE);
				}
			}
			else 
			{
				JOptionPane.showMessageDialog(null, new GSLable("Делить на ноль нельзя", 17), "Ошибка", JOptionPane.ERROR_MESSAGE);
			}
			
		}
		else 
		{
			JOptionPane.showMessageDialog(null, new GSLable("Делить на ноль нельзя", 17), "Ошибка", JOptionPane.ERROR_MESSAGE);
		}
	}
	
    private static float round(float number, int scale) 
    {
    	int pow = 10;
        for (int i = 1; i < scale; i++)
        pow *= 10;
        float tmp = number * pow;
        return (float) (int) ((tmp - (int) tmp) >= 0.5f ? tmp + 1 : tmp) / pow;
    }
}
