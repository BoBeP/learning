package ru.bober.TaskThree;

import javax.swing.JOptionPane;

import ru.bober.GUI.GSLable.GSLable;

public class TaskThree
{

	float d1;
	
	public float x,s,y;
	
	String check = "";
	
	public TaskThree(float a, float b, float x)
	{
		if(a<=b)
		{
			if(!(x == 0))
			{
				s = (float) Math.cos(x);
				
				for(int i = (int) a; i<=b;i++)
				{
					if(!(i == 0))
					{
						s=(float) (s+Math.cos(i*x)/i);
					}
					
				}

				
				d1 = (float) Math.log10(Math.abs(2*Math.sin(x/2)));
				y= (float) (0-(Math.pow(3, x)/d1));
				
				
				if(s>=a && s<=b)
				{
					check = "1";
				}
				else
				{
					check = "0";
				}
				
				s = round(s, 3);
				y = round(y, 3);
			}else
			{
				JOptionPane.showMessageDialog(null, new GSLable("X не может быть равен 0", 17), "Ошибка", JOptionPane.ERROR_MESSAGE);
			}
			
		}
		else
		{
			JOptionPane.showMessageDialog(null, new GSLable("Начало координаты не может быть больше конца", 17), "Ошибка", JOptionPane.ERROR_MESSAGE);
		}
		
	}
	
    private static float round(float number, int scale) 
    {
    	int pow = 10;
        for (int i = 1; i < scale; i++)
        pow *= 10;
        float tmp = number * pow;
        return (float) (int) ((tmp - (int) tmp) >= 0.5f ? tmp + 1 : tmp) / pow;
    }

}
