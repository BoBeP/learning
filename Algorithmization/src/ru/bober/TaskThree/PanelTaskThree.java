package ru.bober.TaskThree;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

import ru.bober.GUI.GSButton.GSButton;
import ru.bober.GUI.GSLable.GSHeaderLable;
import ru.bober.GUI.GSLable.GSLable;
import ru.bober.GUI.GSTextField.GSTextField;

public class PanelTaskThree extends JPanel
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private GSHeaderLable headerLable = new GSHeaderLable("Задание 3", 10, 11, 580, 100, 40);
	
	private GSLable a_lbl = new GSLable("Начало координаты A", 10, 122, 233, 50, 17);
	private GSTextField a_txt = new GSTextField(253, 122, 337, 50, 17);
	private GSLable b_lbl = new GSLable("Конец координаты B", 10, 183, 233, 50, 17);
	private GSTextField b_txt = new GSTextField(253, 183, 337, 50, 17);
	
	private GSLable s_lbl = new GSLable("Результат S", 10, 294, 233, 50, 17);
	private GSLable res_num_s_lbl = new GSLable(253, 294, 168, 50, 17);
	private GSLable res_color_s_lbl = new GSLable(422, 294, 168, 50, 15);
	
	private GSLable y_lbl = new GSLable("Результат Y", 10, 355, 233, 50, 17);
	private GSLable res_y_lbl = new GSLable(253, 355, 337, 50, 17);
	private GSButton button = new GSButton("Получить ответ", 10, 416, 580, 50, 17);
	private GSLable x_lbl = new GSLable("Число X", 10, 244, 233, 50, 17);
	private GSTextField x_txt = new GSTextField(253, 244, 337, 50, 17);

	/**
	 * Create the panel.
	 */
	public PanelTaskThree()
	{
		initialize();
	}
	private void initialize() {
		setPreferredSize(new Dimension(600, 477));
		setLayout(null);
		
		add(headerLable);
		add(a_lbl);
		add(b_lbl);
		add(a_txt);
		add(b_txt);
		add(s_lbl);
		add(res_num_s_lbl);
		add(res_color_s_lbl);
		add(y_lbl);
		add(res_y_lbl);
		add(button);
		add(x_lbl);
		add(x_txt);
		
		button.addActionListener(new BtnListener(a_txt, b_txt, x_txt, res_num_s_lbl, res_color_s_lbl, res_y_lbl));
		
	}
}
 class BtnListener implements ActionListener
 {
	 GSTextField a_txt;
	 GSTextField b_txt;
	 GSTextField x_txt;
	 GSLable res_num_s_lbl;
	 GSLable res_color_s_lbl;
	 GSLable res_y_lbl;

	 float a, b, x;
	public BtnListener(GSTextField a_txt, GSTextField b_txt, GSTextField x_txt, GSLable res_num_s_lbl, GSLable res_color_s_lbl, GSLable res_y_lbl)
	{
		this.a_txt = a_txt;
		this.b_txt = b_txt;
		this.x_txt = x_txt;
		this.res_num_s_lbl = res_num_s_lbl;
		this.res_color_s_lbl = res_color_s_lbl;
		this.res_y_lbl = res_y_lbl;
	}
	 
	@Override
	public void actionPerformed(ActionEvent e)
	{
		
		if(x_txt.getText().equals("") || b_txt.getText().equals("") || a_txt.getText().equals(""))
		{
			
			if(x_txt.getText().equals(""))
			{
				JOptionPane.showMessageDialog(null, new GSLable("Ведите Х", 17), "Ошибка", JOptionPane.ERROR_MESSAGE);
			}
			if(b_txt.getText().equals(""))
			{
				JOptionPane.showMessageDialog(null, new GSLable("Ведите B", 17), "Ошибка", JOptionPane.ERROR_MESSAGE);
			}
			if(a_txt.getText().equals(""))
			{
				JOptionPane.showMessageDialog(null, new GSLable("Ведите A", 17), "Ошибка", JOptionPane.ERROR_MESSAGE);
			}
			
		}else
		{
			a = Float.parseFloat(a_txt.getText());
			b = Float.parseFloat(b_txt.getText());
			x = Float.parseFloat(x_txt.getText());
			
			TaskThree taskThree = new TaskThree(a, b, x);
			
			res_num_s_lbl.setText(String.valueOf(taskThree.s));
			res_y_lbl.setText(String.valueOf(taskThree.y));
			
			res_color_s_lbl.setOpaque(true);
			if(taskThree.check.equals("1"))
			{
				res_color_s_lbl.setBackground(Color.GREEN);
				res_color_s_lbl.setText("Точка входит");
			}else
			{
				res_color_s_lbl.setBackground(Color.RED);
				res_color_s_lbl.setText("Точка Невходит");
			}
		}
		
		
	}
	 
 }