package ru.bober.TaskFive;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

import ru.bober.GUI.GSButton.GSButton;
import ru.bober.GUI.GSLable.GSHeaderLable;
import ru.bober.GUI.GSLable.GSLable;
import ru.bober.GUI.GSTextField.GSTextField;

public class PanelTaskFive extends JPanel
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private GSHeaderLable headerLable = new GSHeaderLable("Задание 5", 10, 11, 580, 80, 40);
	private GSLable line_lbl = new GSLable("Количество строк", 10, 100, 212, 50, 17);
	private GSTextField line_txt = new GSTextField(261, 102, 329, 48, 17);
	private GSLable column_lbl = new GSLable("Количество колонок", 10, 161, 212, 50, 17) ;
	private GSTextField column_txt = new GSTextField(261, 161, 329, 50, 17);
	private GSLable start_lbl = new GSLable("Начало рандома", 10, 222, 212, 50, 17);
	private GSTextField start_txt = new GSTextField(261, 222, 329, 50, 17);
	private GSLable end_lbl = new GSLable("Конец рандома", 10, 283, 212, 50, 17);
	private GSTextField end_txt = new GSTextField(261, 283, 329, 50, 17);
	private GSButton button = new GSButton("Вычеслить результат", 10, 350, 580, 116, 17);

	/**
	 * Create the panel.
	 */
	public PanelTaskFive()
	{
		initialize();
	}
	private void initialize() {
		setPreferredSize(new Dimension(600, 477));
		setLayout(null);
		
		add(headerLable);
		add(line_lbl);
		add(line_txt);
		add(column_lbl);
		add(column_txt);
		add(start_lbl);
		add(start_txt);
		add(end_lbl);
		add(end_txt);
		add(button);
		button.addActionListener(new BtnListener(line_txt, column_txt, start_txt, end_txt));
	}
}
class BtnListener implements ActionListener 
{
	GSTextField line_txt;
	GSTextField column_txt;
	GSTextField start_txt;
	GSTextField end_txt;
	int line,  column,  start,  end;
	public BtnListener(GSTextField line_txt, GSTextField column_txt, GSTextField start_txt, GSTextField end_txt)
	{
		this.line_txt = line_txt;
		this.column_txt = column_txt;
		this.start_txt = start_txt;
		this.end_txt = end_txt;
	}
	
	@Override
	public void actionPerformed(ActionEvent e)
	{
		line = Integer.parseInt(line_txt.getText());
		column = Integer.parseInt(column_txt.getText());
		start = Integer.parseInt(start_txt.getText());
		end = Integer.parseInt(end_txt.getText());

		try
		{
			@SuppressWarnings("unused")
			TaskFive taskFive = new TaskFive(line, column, start, end);
			JOptionPane.showMessageDialog(null, new GSLable("Результат должны быть по адресу С:\\BoBeP\\TaskFive.txt", 17) , "Ошибка", JOptionPane.ERROR_MESSAGE);
		} catch (FileNotFoundException e1)
		{
			String ttt = "Не найден файл по пути C:\\BoBeP\\TaskFive.txt\r\n"
					+ "Пожалуйста временно создайте его.\r\n"
					+ "Для просмотра лучше всего использовать NotePad++.\r\n"
					+ "Eсли появилось ошибка с кодировкой то сменить её либо на UTF-8, либо на  ANSI";
			
			
				      	
				JOptionPane.showMessageDialog(null, ttt , "Ошибка", JOptionPane.ERROR_MESSAGE);
			
			//e1.printStackTrace();
		}
		
		
	}
}