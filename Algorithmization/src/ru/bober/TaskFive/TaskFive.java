package ru.bober.TaskFive;

import java.io.FileNotFoundException;
import java.security.SecureRandom;

import ru.bober.FileWorker;

public class TaskFive
{

	private static String fileName = "C://BoBeP//TaskFive.txt";
	private float num_min, num_max;
	public TaskFive(int line, int column, int start, int end) throws FileNotFoundException
	{
		String newText = "";
		float [][] massiv = new float[line][column];
		float num;
		SecureRandom secureRandom = new SecureRandom();
		for (int i = 0; i < line; i++) 
		{
		    for (int j = 0; j < column; j++) 
		    {
		    	num = secureRandom.nextFloat()*(end - start)  + start;
		    	massiv[i][j] = round(num, 2);
		    	newText = newText +massiv[i][j] + "\t";
		    }
		    FileWorker.update(fileName, newText);
		    newText = "";
		}
		newText = " ";
		FileWorker.update(fileName, newText);
	   
		
		
		
		int maxPoz_column = 1;
		int maxPoz_line = 1;
		int minPoz_column = 1;
		int minPoz_line = 1;
		
		for (int i = 0; i < line; i++) 
		{
			System.out.println(i);
			if(i % 2 == 0)
			{
				num_max= massiv[i][0];
				for (int j = 0; j < column; j++) 
				{
					if (massiv[i][j] > num_max) 
					{
						num_max = massiv[i][j];
						maxPoz_column = 1 + j;
					} 
				}
				newText = "Максимальный элемент массива "+num_max+" находится в "+ (i+1) + " строке и в " +  maxPoz_column + " колонке\n";
				FileWorker.update(fileName, newText);
				
				maxPoz_column = 0;
			}else
			{
				num_min = massiv[i][0];
				for (int j = 0; j < column; j++) 
				{
					if (massiv[i][j] < num_min) 
					{
						num_min = massiv[i][j];
						minPoz_column = 1 + j;
					} 
				}
				
				newText = "Минимальный элемент массива "+num_min+" находится в "+ (i+1) + " строке и в " +  minPoz_column + " колонке\n";
				FileWorker.update(fileName, newText);
				
				minPoz_column = 0;
			}
		}
		
		newText = " ";
		FileWorker.update(fileName, newText);
		
		num_min = massiv[0][0];
		num_max= massiv[0][0];
		
		for (int i = 0; i < line; i++) 
		{
		    for (int j = 0; j < column; j++) 
		    {
		    	if (massiv[i][j] < num_min) 
				{
					num_min = massiv[i][j];
					minPoz_column = j+1;
					minPoz_line = 1+i;
				} 
		    	if (massiv[i][j] > num_max) 
				{
					num_max = massiv[i][j];
					maxPoz_column = j+1;
					maxPoz_line = 1+i;
				} 
		    }
		    
		}
		
		newText = "Максимальный элемент массива " + num_max + " находится в "+ maxPoz_line + " строке и в " +  maxPoz_column + " колонке\n";
		FileWorker.update(fileName, newText);
		
		newText = "Минимальный элемент массива " + num_min + " находится в "+ minPoz_line + " строке и в " +  minPoz_column + " колонке\n";
		FileWorker.update(fileName, newText);
		
		newText = "Сумма максимального и минимального элемента массива: " +round((num_min + num_max),2) + "\n";
		FileWorker.update(fileName, newText);
		
		newText = " ";
		FileWorker.update(fileName, newText);
		
		newText = "Удаляем строку и столбец с наибольшим значаением\n";
		FileWorker.update(fileName, newText);
		newText ="";
		for (int i = 0; i < line; i++) 
		{
		    for (int j = 0; j < column; j++) 
		    {
		    	if(!(i == (maxPoz_line-1)) && !(j == (maxPoz_column-1)))
		    	{
		    		newText = newText +massiv[i][j] + "\t";
		    	}
		    }
		    FileWorker.update(fileName, newText);
		    newText = "";
		}
		FileWorker.update(fileName, newText);
	    newText = "";
	}
	
	 private static float round(float number, int scale) 
	 {
	    	int pow = 10;
	        for (int i = 1; i < scale; i++)
	        pow *= 10;
	        float tmp = number * pow;
	        return (float) (int) ((tmp - (int) tmp) >= 0.5f ? tmp + 1 : tmp) / pow;
	 }

}
