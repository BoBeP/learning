package ru.bober;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

import de.javasoft.plaf.synthetica.SyntheticaPlainLookAndFeel;
import ru.bober.GUI.GSTabbedPane.GSTabs;
import ru.bober.TaskFive.PanelTaskFive;
import ru.bober.TaskFour.PanelTaskFour;
import ru.bober.TaskOne.PanelTaskOne;
import ru.bober.TaskSix.PanelTaskSix;
import ru.bober.TaskThree.PanelTaskThree;
import ru.bober.TaskTwo.PanelTaskTwo;

public class Start extends JFrame
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6184232057388664757L;
	private JPanel contentPane;
	private GSTabs tabs;
	private PanelTaskOne panelTaskOne;
	private PanelTaskTwo panelTaskTwo;
	private PanelTaskThree panelTaskThree;
	private PanelTaskFour panelTaskFour;
	private PanelTaskFive panelTaskFive;
	private PanelTaskSix panelTaskSix;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args)
	{
		EventQueue.invokeLater(new Runnable()
		{
			public void run()
			{
				try
				{
					UIManager.setLookAndFeel(new SyntheticaPlainLookAndFeel());	
					Start frame = new Start();
					frame.setVisible(true);
					frame.setIconImage(Toolkit.getDefaultToolkit().getImage(Start.class.getResource("/resources/image/icon/icon.png")));
					frame.setResizable(false);
					Font font = Font.createFont(Font.TRUETYPE_FONT, getClass().getResource("/resources/font/Text/al35HDUI.ttf").openStream());
					GraphicsEnvironment genv = GraphicsEnvironment.getLocalGraphicsEnvironment();
					genv.registerFont(font);
					font = font.deriveFont(Font.BOLD, 17f);
					UIManager.put("OptionPane.messageFont", font);
					
				} catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Start()
	{
		initialize();
	}
	private void initialize() {
		setTitle("Задание по паскаль");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		super.setLocationRelativeTo(null);
		setBounds(100, 100, 641, 560);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		tabs = new GSTabs();
		contentPane.add(tabs, BorderLayout.CENTER);
		
		panelTaskOne = new PanelTaskOne();
		tabs.addTab("Задание 1", null, panelTaskOne, null);
		
		panelTaskTwo = new PanelTaskTwo();
		tabs.addTab("Задание 2", null, panelTaskTwo, null);
		
		panelTaskThree = new PanelTaskThree();
		tabs.addTab("Задание 3", null, panelTaskThree, null);
		
		panelTaskFour = new PanelTaskFour();
		tabs.addTab("Задание 4", null, panelTaskFour, null);
		
		panelTaskFive = new PanelTaskFive();
		tabs.addTab("Задание 5", null, panelTaskFive, null);
		
		panelTaskSix = new PanelTaskSix();
		tabs.addTab("Задание 6", null, panelTaskSix, null);
	}
}
