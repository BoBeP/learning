package ru.bober.TaskFour;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

import ru.bober.GUI.GSButton.GSButton;
import ru.bober.GUI.GSLable.GSHeaderLable;
import ru.bober.GUI.GSLable.GSLable;
import ru.bober.GUI.GSTextField.GSTextField;

public class PanelTaskFour extends JPanel
{
	
	private static final long serialVersionUID = 1L;
	private GSHeaderLable headerLable = new GSHeaderLable("Задание 4", 10, 11, 580, 80, 40);
	private GSLable start_lbl = new GSLable("Начало диапазона генерации чисел", 10, 102, 580, 50, 17);
	private GSTextField start_txt = new GSTextField(10, 163, 580, 50, 17);
	private GSLable end_lbl = new GSLable("Конец диапазона генерации чисел", 10, 224, 580, 50, 17);
	private GSTextField end_txt = new GSTextField(10, 285, 580, 50, 17);
	private GSButton button = new GSButton("Вычеслить", 10, 346, 580, 120, 17);

	/**
	 * Create the panel.
	 */
	public PanelTaskFour()
	{
		initialize();
	}
	private void initialize() {
		setPreferredSize(new Dimension(600, 477));
		setLayout(null);
		
		add(headerLable);
		add(start_lbl);
		add(start_txt);
		add(end_lbl);
		add(end_txt);
		add(button);
		button.addActionListener(new BtnListener(start_txt, end_txt));
	}
}
class BtnListener implements ActionListener
{
	GSTextField start_txt;
	GSTextField end_txt;
	public BtnListener(GSTextField start_txt, GSTextField end_txt)
	{
		this.start_txt = start_txt;
		this.end_txt = end_txt;
	}
	int start, end;
	
	@Override
	public void actionPerformed(ActionEvent e)
	{
		start =Integer.parseInt(start_txt.getText());
		end = Integer.parseInt(end_txt.getText());
		
		
		try
		{
			@SuppressWarnings("unused")
			TaskFour taskFour = new TaskFour(start, end);
			JOptionPane.showMessageDialog(null, new GSLable("Результат должны быть по адресу С:\\BoBeP\\TaskFour.txt", 17) , "Ошибка", JOptionPane.ERROR_MESSAGE);
		} catch (FileNotFoundException e1)
		{
			String ttt = "Не найден файл по пути C:\\BoBeP\\TaskFour.txt\r\n"
					+ "Пожалуйста временно создайте его.\r\n"
					+ "Для просмотра лучше всего использовать NotePad++.\r\n"
					+ "Eсли появилось ошибка с кодировкой то сменить её либо на UTF-8, либо на  ANSI";
			
			
				      	
				JOptionPane.showMessageDialog(null, ttt , "Ошибка", JOptionPane.ERROR_MESSAGE);
			
			//e1.printStackTrace();
		}
		
		
	}
	
}