package ru.bober.TaskFour;

import java.io.FileNotFoundException;
import java.security.SecureRandom;

import ru.bober.FileWorker;

public class TaskFour
{

	float chislo;
	float plus = 0;
	float minus = 0;
	
	int amount_plus = 0;
	int amount_minus = 0;
	
	int amount_aneven = 0;
	int amount_odd = 0;
	float res_plus;
	float res_minus;
	
	private static String fileName = "C://BoBeP//TaskFour.txt";
	
	public TaskFour(int start, int end) throws FileNotFoundException
	{
		int a = (end - start)+1;
		SecureRandom secureRandom = new SecureRandom();
		
		for(int i = 0; i < a ;i++)
		{
			chislo = secureRandom.nextInt((end - start) + 1) + start;
			
			if(chislo >= 0)
			{
				if(chislo % 2 == 0) 
				 {
					res_plus = (float) Math.sin(Math.pow(chislo, 2)-1);//Хз чё это но без этого будет не правильно 
					String newText = "Чётный X" +"(" + Double.toString(chislo)+ ")"  + " = " + res_plus +"\n";
					FileWorker.update(fileName, newText);
					amount_aneven++;//количество четных
				 } 
				 else 
				 {
					
					 res_plus = (float) (Math.pow(chislo, 3)+Math.pow(chislo, 2) + chislo + 1);//Хз чё это но без этого будет не правильно 
					 String newText = "Нечётный X" +"(" + Double.toString(chislo)+ ")"  + " = " + res_plus +"\n";
					 FileWorker.update(fileName, newText);
					 amount_odd++;//Количество не четных
					 
				 }
				amount_plus++;//Количество положительных чисел
				plus = plus + chislo;//Сумма положительных чисел

			}
			else 
			{
				
				if(chislo % 2 == 0) 
				 {
					res_minus = (float) Math.sin(Math.pow(chislo, 2)-1);
					String newText = "Чётный X" +"(" + Double.toString(chislo)+ ")"  + " = " + res_minus +"\n";
					FileWorker.update(fileName, newText);
					amount_aneven++;//количество четных
				 } 
				 else 
				 {
					
					 res_minus = (float) (Math.pow(chislo, 3)+Math.pow(chislo, 2) + chislo + 1);
					 String newText = "Нечётный X" +"(" + Double.toString(chislo)+ ")"  + " = " + res_minus +"\n";
					 FileWorker.update(fileName, newText);
					 amount_odd++;//Количество не четных
				 }
				amount_minus++;
				minus = minus + chislo;
			}
		}
		
		String newText1 = "Сумма положительных чисел: " + plus
				+ "\nКоличесвто положительных чисел: " +amount_plus +"\n";
		FileWorker.update(fileName, newText1);
		
		String newText2 = "Сумма отрицательных чисел чисел: " + minus
				+ "\nКоличесвто отрицательных чисел: " +amount_minus +"\n ";
		FileWorker.update(fileName, newText2);
		
		String newText3 = "Количестов четных чисел: " + amount_aneven
				+ "\nКоличесвто не четных чисел чисел: " +amount_odd +"\n ";
		FileWorker.update(fileName, newText3);
	}

}
