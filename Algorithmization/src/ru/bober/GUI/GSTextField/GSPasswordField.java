package ru.bober.GUI.GSTextField;

import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.GraphicsEnvironment;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

import javax.swing.JPasswordField;

/**
 * 
 * @author BoBeP
 * @version 0.1
 */
public class GSPasswordField extends JPasswordField
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public GSPasswordField()
	{
		Font newFont;
		try
        {
			Font font = Font.createFont(Font.TRUETYPE_FONT, getClass().getResource("/resources/font/Text/al35HDUI.ttf").openStream());
			
			GraphicsEnvironment genv = GraphicsEnvironment.getLocalGraphicsEnvironment();
			genv.registerFont(font);
			// makesure to derive the size
			font = font.deriveFont(Font.BOLD, 17f);
			
	        super.setFont(font);
        } catch (FontFormatException | IOException ex)
        {
        	ex.printStackTrace();
        }
	}
	public GSPasswordField(int x, int y, int width, int height)
	{
		Font newFont;
		try
        {
			Font font = Font.createFont(Font.TRUETYPE_FONT, getClass().getResource("/resources/font/Text/al35HDUI.ttf").openStream());
			
			GraphicsEnvironment genv = GraphicsEnvironment.getLocalGraphicsEnvironment();
			genv.registerFont(font);
			// makesure to derive the size
			font = font.deriveFont(Font.BOLD, 17f);
			
	        super.setFont(font);
        } catch (FontFormatException | IOException ex)
        {
        	ex.printStackTrace();
        }
		super.setBounds(x, y, width, height);
	}

}

