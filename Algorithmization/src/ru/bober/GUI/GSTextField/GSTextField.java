package ru.bober.GUI.GSTextField;

import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.GraphicsEnvironment;
import java.io.IOException;

import javax.swing.JTextField;

/**
 * 
 * @author BoBeP
 * @version 0.2
 */
public class GSTextField extends JTextField
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public GSTextField()
	{
		try
        {
			Font font = Font.createFont(Font.TRUETYPE_FONT, getClass().getResource("/resources/font/Text/al35HDUI.ttf").openStream());
			
			GraphicsEnvironment genv = GraphicsEnvironment.getLocalGraphicsEnvironment();
			genv.registerFont(font);
			// makesure to derive the size
			font = font.deriveFont(Font.BOLD, 17f);
			
	        super.setFont(font);
        } catch (FontFormatException | IOException ex)
        {
        	ex.printStackTrace();
        }
	}
	
	public GSTextField(int x, int y, int width, int height, float textsize)
	{
		try
        {
			Font font = Font.createFont(Font.TRUETYPE_FONT, getClass().getResource("/resources/font/Text/al35HDUI.ttf").openStream());
			
			GraphicsEnvironment genv = GraphicsEnvironment.getLocalGraphicsEnvironment();
			genv.registerFont(font);
			// makesure to derive the size
			font = font.deriveFont(Font.BOLD, textsize);
			
	        super.setFont(font);
        } catch (FontFormatException | IOException ex)
        {
        	ex.printStackTrace();
        }
		super.setBounds(x, y, width, height);
	}

}
