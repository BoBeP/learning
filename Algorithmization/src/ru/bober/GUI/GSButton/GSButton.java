package ru.bober.GUI.GSButton;

import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.GraphicsEnvironment;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

import javax.swing.JButton;

/**
 * 
 * @author BoBeP
 * @version 0.2
 */
public class GSButton extends JButton
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public GSButton()
	{
		Font newFont;
		try
        {
			File fontFile = new File(this.getClass().getResource("/resources/font/Text/al35HDUI.ttf").toURI());
	        newFont = Font.createFont(Font.TRUETYPE_FONT, fontFile).deriveFont(Font.BOLD, 17f);
	        super.setFont(newFont);
        } catch (FontFormatException | URISyntaxException | IOException ex)
        {
        	ex.printStackTrace();
        }
	}

	/**
	 * @param text текст который будет на кнопке
	 * @param x расположение по X
	 * @param y расположение по Y
	 * @param width длина
	 * @param height высота
	 * @param sizee размер текста
	 */
	public GSButton(String text, int x, int y, int width, int height, float textsize)
	{
		super(text);
		Font newFont;
		try
        {
			Font font = Font.createFont(Font.TRUETYPE_FONT, getClass().getResource("/resources/font/Text/al35HDUI.ttf").openStream());
			
			GraphicsEnvironment genv = GraphicsEnvironment.getLocalGraphicsEnvironment();
			genv.registerFont(font);
			// makesure to derive the size
			font = font.deriveFont(Font.BOLD, textsize);
			
	        super.setFont(font);
        } catch (FontFormatException | IOException ex)
        {
        	ex.printStackTrace();
        }
		super.setBounds(x, y, width, height);
	}
	
	public GSButton(String text, float textsize)
	{
		super(text);
		Font newFont;
		try
        {
			Font font = Font.createFont(Font.TRUETYPE_FONT, getClass().getResource("/resources/font/Text/al35HDUI.ttf").openStream());
			
			GraphicsEnvironment genv = GraphicsEnvironment.getLocalGraphicsEnvironment();
			genv.registerFont(font);
			// makesure to derive the size
			font = font.deriveFont(Font.BOLD, textsize);
			
	        super.setFont(font);
        } catch (FontFormatException | IOException ex)
        {
        	ex.printStackTrace();
        }
	}
}
