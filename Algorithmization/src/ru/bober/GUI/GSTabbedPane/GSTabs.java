package ru.bober.GUI.GSTabbedPane;

import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.GraphicsEnvironment;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

import javax.swing.JTabbedPane;

/**
 * 
 * @author BoBeP
 * @version 0.1
 */
public class GSTabs extends JTabbedPane
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public GSTabs()
	{
		Font newFont;
		try
        {
			Font font = Font.createFont(Font.TRUETYPE_FONT, getClass().getResource("/resources/font/Text/al35HDUI.ttf").openStream());
			
			GraphicsEnvironment genv = GraphicsEnvironment.getLocalGraphicsEnvironment();
			genv.registerFont(font);
			// makesure to derive the size
			font = font.deriveFont(Font.BOLD, 12f);
			
	        super.setFont(font);
        } catch (FontFormatException | IOException ex)
        {
        	ex.printStackTrace();
        }
	}
	public GSTabs(int x, int y, int width, int height)
	{
		Font newFont;
		try
        {
			Font font = Font.createFont(Font.TRUETYPE_FONT, getClass().getResource("/resources/font/Text/al35HDUI.ttf").openStream());
			
			GraphicsEnvironment genv = GraphicsEnvironment.getLocalGraphicsEnvironment();
			genv.registerFont(font);
			// makesure to derive the size
			font = font.deriveFont(Font.BOLD, 12f);
			
	        super.setFont(font);
        } catch (FontFormatException | IOException ex)
        {
        	ex.printStackTrace();
        }
		super.setBounds(x, y, width, height);
	}

	public GSTabs(int tabPlacement)
	{
		super(tabPlacement);
		// TODO Автоматически созданная заглушка конструктора
	}

	public GSTabs(int tabPlacement, int tabLayoutPolicy)
	{
		super(tabPlacement, tabLayoutPolicy);
		// TODO Автоматически созданная заглушка конструктора
	}

}

