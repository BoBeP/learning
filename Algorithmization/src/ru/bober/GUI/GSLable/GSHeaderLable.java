package ru.bober.GUI.GSLable;

import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.GraphicsEnvironment;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

import javax.swing.JLabel;
import javax.swing.SwingConstants;

/**
 * 
 * @author BoBeP
 * @version 0.2
 */
public class GSHeaderLable extends JLabel
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public GSHeaderLable()
	{
		try 
		{
			
			Font font = Font.createFont(Font.TRUETYPE_FONT, getClass().getResource("/resources/font/Header/Pu6ZA5Zi.ttf").openStream());
			
			GraphicsEnvironment genv = GraphicsEnvironment.getLocalGraphicsEnvironment();
			genv.registerFont(font);
			// makesure to derive the size
			font = font.deriveFont(Font.BOLD, 40f);
			
	        super.setFont(font);
		} catch (FontFormatException | IOException ex)
        {
			ex.printStackTrace();
        }
		super.setHorizontalAlignment(SwingConstants.CENTER);
		super.setVerticalAlignment(SwingConstants.CENTER);
	}
	
	public GSHeaderLable(String text)
	{
		super(text);
		Font newFont;
		try 
		{
			Font font = Font.createFont(Font.TRUETYPE_FONT, getClass().getResource("/resources/font/Header/Pu6ZA5Zi.ttf").openStream());
			
			GraphicsEnvironment genv = GraphicsEnvironment.getLocalGraphicsEnvironment();
			genv.registerFont(font);
			// makesure to derive the size
			font = font.deriveFont(Font.BOLD, 40f);
			
	        super.setFont(font);
		} catch (FontFormatException | IOException ex)
        {
			ex.printStackTrace();
        }
		super.setHorizontalAlignment(SwingConstants.CENTER);
		super.setVerticalAlignment(SwingConstants.CENTER);
	}
	/**
	 * 
	 * @param text текст который надо вывести
	 * @param x расположение по X
	 * @param y расположение по Y
	 * @param width длина
	 * @param height высота
	 * @param sizee размер текста
	 */
	public GSHeaderLable(String text, int x, int y, int width, int height, float textsize)
	{
		super(text);
		Font newFont;
		try 
		{
			Font font = Font.createFont(Font.TRUETYPE_FONT, getClass().getResource("/resources/font/Header/Pu6ZA5Zi.ttf").openStream());
			
			GraphicsEnvironment genv = GraphicsEnvironment.getLocalGraphicsEnvironment();
			genv.registerFont(font);
			// makesure to derive the size
			font = font.deriveFont(Font.BOLD, textsize);
			
	        super.setFont(font);
		} catch (FontFormatException | IOException ex)
        {
			ex.printStackTrace();
        }
		super.setBounds(x, y, width, height);
		
		super.setHorizontalAlignment(SwingConstants.CENTER);
		super.setVerticalAlignment(SwingConstants.CENTER);
	}
}

