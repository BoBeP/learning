package ru.bober.GUI.GSLable;

import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.GraphicsEnvironment;
import java.io.IOException;

import javax.swing.JLabel;

/**
 * @author BoBeP
 * @version 0.2
 */
public class GSLable extends JLabel
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	
	public GSLable()
	{
		Font newFont;
		try
        {
			Font font = Font.createFont(Font.TRUETYPE_FONT, getClass().getResource("/resources/font/Text/al35HDUI.ttf").openStream());
			
			GraphicsEnvironment genv = GraphicsEnvironment.getLocalGraphicsEnvironment();
			genv.registerFont(font);
			// makesure to derive the size
			font = font.deriveFont(Font.BOLD, 17f);
			
	        super.setFont(font);
        } catch (FontFormatException | IOException ex)
        {
        	ex.printStackTrace();
        }
	}
	
	public GSLable(String text, float textsize)
	{
		super(text);
		Font newFont;
		
		try
        {
			Font font = Font.createFont(Font.TRUETYPE_FONT, getClass().getResource("/resources/font/Text/al35HDUI.ttf").openStream());
			
			GraphicsEnvironment genv = GraphicsEnvironment.getLocalGraphicsEnvironment();
			genv.registerFont(font);
			// makesure to derive the size
			font = font.deriveFont(Font.BOLD, textsize);
			
	        super.setFont(font);
        } catch (FontFormatException | IOException ex)
        {
        	ex.printStackTrace();
        }
	}
	
	/**
	 * @param text текст который надо вывести
	 * @param x расположение по X
	 * @param y расположение по Y
	 * @param width длина
	 * @param height высота
	 * @param sizee размер текста
	 */
	public GSLable(String text, int x, int y, int width, int height, float textsize)
	{
		super(text);
		Font newFont;
		try
        {
			Font font = Font.createFont(Font.TRUETYPE_FONT, getClass().getResource("/resources/font/Text/al35HDUI.ttf").openStream());
			
			GraphicsEnvironment genv = GraphicsEnvironment.getLocalGraphicsEnvironment();
			genv.registerFont(font);
			// makesure to derive the size
			font = font.deriveFont(Font.BOLD, textsize);
			
	        super.setFont(font);
        } catch (FontFormatException | IOException ex)
        {
        	ex.printStackTrace();
        }
		super.setBounds(x, y, width, height);
	}
	
	public GSLable(int x, int y, int width, int height, float textsize)
	{
		Font newFont;
		try
        {
			Font font = Font.createFont(Font.TRUETYPE_FONT, getClass().getResource("/resources/font/Text/al35HDUI.ttf").openStream());
			
			GraphicsEnvironment genv = GraphicsEnvironment.getLocalGraphicsEnvironment();
			genv.registerFont(font);
			// makesure to derive the size
			font = font.deriveFont(Font.BOLD, textsize);
			
	        super.setFont(font);
        } catch (FontFormatException | IOException ex)
        {
        	ex.printStackTrace();
        }
		super.setBounds(x, y, width, height);
	}
}
