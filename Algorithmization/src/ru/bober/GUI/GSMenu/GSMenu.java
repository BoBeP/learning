package ru.bober.GUI.GSMenu;

import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.GraphicsEnvironment;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

import javax.swing.Action;
import javax.swing.JMenu;

public class GSMenu extends JMenu
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public GSMenu()
	{
		Font newFont;
		try
        {
			Font font = Font.createFont(Font.TRUETYPE_FONT, getClass().getResource("/resources/font/Text/al35HDUI.ttf").openStream());
			
			GraphicsEnvironment genv = GraphicsEnvironment.getLocalGraphicsEnvironment();
			genv.registerFont(font);
			// makesure to derive the size
			font = font.deriveFont(Font.BOLD, 17f);
			
	        super.setFont(font);
        } catch (FontFormatException | IOException ex)
        {
        	ex.printStackTrace();
        }
	}

	public GSMenu(String s)
	{
		super(s);
		Font newFont;
		try
        {
			Font font = Font.createFont(Font.TRUETYPE_FONT, getClass().getResource("/resources/font/Text/al35HDUI.ttf").openStream());
			
			GraphicsEnvironment genv = GraphicsEnvironment.getLocalGraphicsEnvironment();
			genv.registerFont(font);
			// makesure to derive the size
			font = font.deriveFont(Font.BOLD, 17f);
			
	        super.setFont(font);
        } catch (FontFormatException | IOException ex)
        {
        	ex.printStackTrace();
        }
	}

	public GSMenu(Action a)
	{
		super(a);
		// TODO Автоматически созданная заглушка конструктора
	}

	public GSMenu(String s, boolean b)
	{
		super(s, b);
		// TODO Автоматически созданная заглушка конструктора
	}

}
